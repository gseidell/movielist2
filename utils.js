var fs = require('fs');

// function restoreOriginalData() {
//     fs.writeFileSync('poke.json', fs.readFileSync('poke_original.json'));
// }

function loadData() {
    return JSON.parse(fs.readFileSync('movieData.json'));
}

function saveData(data) {
    var obj = {
        movies: data
    };

    fs.writeFileSync('movieData.json', JSON.stringify(obj));
}

module.exports = {
    // restoreOriginalData: restoreOriginalData,
    loadData: loadData,
    saveData: saveData,
}
