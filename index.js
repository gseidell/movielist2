const request = require('request');
var express = require('express');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');

var dataUtil = require("./utils");
var _DATA = dataUtil.loadData().movies;

var app = express();

//Path dictated by nginx configuration
//make "" for testing with localhost
var path = "/movies";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.engine('handlebars', exphbs({ defaultLayout: 'header' }));
app.set('view engine', 'handlebars');
app.use(path+'/public', express.static('public'));


app.get(path+'/', function (req, res) {
    res.render('list', { data: _DATA });
})

//app.get('/tmdbLogo.png', function (req, res) {
//    res.sendFile("public/tmdbLogo.png", { root: __dirname });
//})

app.post(path+'/api/addMovie', function (req, res) {
    var body = req.body;

    //console.log(body);

    if (body.api == "on") {
        var getRequest =
            "https://api.themoviedb.org/3/search/movie?api_key=39da636eab3951b9d1cd4e1f623dbdc4&language=en-US&query=";
        getRequest += encodeURI(body.title);
        waiting = true;
        request(getRequest, { json: true }, (err, response, body2) => {
            if (err) { return console.log(err); }
            var result = body2.results[0];
            var newEntry = {
                title: result.title,
                poster_path: result.poster_path,
                overview: result.overview,
                year: result.release_date.substring(0, 4),
                name: body.name,
            };
            _DATA.unshift(newEntry);
            dataUtil.saveData(_DATA);
            res.redirect(path+"/");
        });
    } else {
        var newEntry = {
            title: body.title,
            poster_path: "",
            overview: "",
            year: body.year,
            name: body.name,
        };
        // Save new post
        _DATA.unshift(newEntry);
        dataUtil.saveData(_DATA);
        res.redirect(path+"/");
    }

});


app.post(path+'/api/removeMovie', function (req, res) {
    var body = req.body;

    newData = _DATA.filter(x => x.title != body.titleToRemove);

    dataUtil.saveData(newData);
    _DATA = newData;
    res.redirect(path+"/");

});


app.listen(3000, function () {
    console.log('Listening on port 3000!');
});
